package com.example.ejemplo02android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnProcesar.setOnClickListener(){
            val edad: Int=txtEdad.text.toString().toInt()
            if (edad>=18){
                this.txtResultado.text= getResources().getString(R.string.rslMayor);
            }else{
                this.txtResultado.text= getResources().getString(R.string.rslMenor);
            }
        }
    }
}